package events

// Source of an event.
type EventSource interface {
	Attach(EventListener)
	NotifyListeners(EventType, EventData)
}
