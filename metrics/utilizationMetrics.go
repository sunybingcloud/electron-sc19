package metrics

import (
	"bitbucket.org/sunybingcloud/electron/events"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/utilities/runAvg"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"log"
	"math"
	"os"
	"reflect"
	"strconv"
	"strings"
	"sync"
)

type UtilizationMetricsListener struct {
	eventTypes []events.EventType
	sync.Mutex
	// PCP data headers.
	// Mapping hostname to the pcp data tag to the indices in the header.
	// Example {host: myHost => {tag: 'kernel.all.cpu.user' => indices: [x, y, z ...]}}
	PCPHeaders map[string]map[string][]int

	// Power usage information for each node in the cluster.
	// All values are stored in Watts.
	PowerUsageCluster map[string]*powerUsageNode

	// CPU usage information for each node in the cluster.
	// All values are represented in %.
	CpuUsageCluster map[string]*cpuUsageNode

	// Send the type of message to be logged.
	logMsgType chan elecLogDef.LogMessageType
	// Send the message to be logged.
	logMsg chan string

	logsPersisted bool

	eventListeners map[events.EventType][]events.EventListener
}

// runAvg.Interface type to embed data for calculating historic power usage average.
type powerData struct {
	val *float64
}

func (p *powerData) Val() float64 {
	return *p.val
}

func (p *powerData) ID() string {
	// Dummy implementation.
	return fmt.Sprintf("%f", *p.val)
}

// Metrics corresponding to power usage for a given host.
// All power consumption data is expressed in Watts.
type powerUsageNode struct {
	// POWER SPECIFICATIONS.
	// CPU power consumption when CPU is 100% idle.
	IdlePowerWattsCpu float64 `json:"idle_power_watts_cpu"`
	// DRAM power consumption when DRAM is 100% idle.
	IdlePowerWattsDram float64 `json:"idle_power_watts_dram"`
	// Thermal design power.
	TDPWatts float64 `json:"tdp_watts"`
	// Maximum available power (max power (cpu + dram)).
	MaxCpuDramPowerWatts float64 `json:"max_cpu_dram_power_watts"`

	// POWER CONSUMPTION INFORMATION.
	// Total power consumed by CPU (Watts).
	totalPowerCpu float64
	// Total power consumed by DRAM (Watts).
	totalPowerDram float64
	// Historic power consumption.
	historicPowerConsumptionCpu  []float64
	historicPowerConsumptionDram []float64
	// Storing average of the last N seconds of CPU power usage.
	histAvgPowerCpu float64

	ravg *runAvg.RunAvgCalculator
}

func (p *powerUsageNode) update(totalCpuPowerRaw float64, totalDramPowerRaw float64) (float64, float64) {
	// Converting to Watts.
	p.totalPowerCpu = totalCpuPowerRaw * math.Pow(2, -32)
	p.totalPowerDram = totalDramPowerRaw * math.Pow(2, -32)
	// Adding to power consumption information to history.
	p.historicPowerConsumptionCpu = append(p.historicPowerConsumptionCpu, p.totalPowerCpu)
	p.historicPowerConsumptionDram = append(p.historicPowerConsumptionDram, p.totalPowerDram)
	// Updating running average of past WINDOW_RUN_AVERAGE_POWER_HIST readings.
	p.histAvgPowerCpu = p.ravg.Calc(&powerData{val: &p.totalPowerCpu})

	return p.totalPowerCpu, p.totalPowerDram
}

func (p *powerUsageNode) String() string {
	values := []string{
		strconv.FormatFloat(p.totalPowerCpu, 'f', 3, 64),
		strconv.FormatFloat(p.totalPowerDram, 'f', 3, 64),
	}

	return strings.Join(values, ",")
}

// runAvg.Interface type to embed data for calculating historic cpu utilization average.
type cpuUsageData struct {
	val *float64
}

func (c *cpuUsageData) Val() float64 {
	return *c.val
}

func (c *cpuUsageData) ID() string {
	// Dummy implementation.
	return fmt.Sprintf("%f", *c.val)
}

// Metrics corresponding to cpu usage for a given host.
type cpuUsageNode struct {
	// CPU utilization.
	cpuUtilization float64
	// Historic CPU utilization.
	historicCpuUtilization []float64
	// Storing average of the last N seconds of CPU utilization.
	histAvgCpuUtil float64

	ravg *runAvg.RunAvgCalculator
}

func (c *cpuUsageNode) update(userCpuTime float64, sysCpuTime float64, idleCpuTime float64) float64 {
	// Updating the latest cpuUtilization value.
	// CPU utilization (Cu) = cpu_time / (cpu_time + idle_time)
	c.cpuUtilization = (userCpuTime + sysCpuTime) / (userCpuTime + sysCpuTime + idleCpuTime)

	// Adding cpu utilization to history.
	c.historicCpuUtilization = append(c.historicCpuUtilization, c.cpuUtilization)
	// Updating running average of past WINDOW_RUN_AVERAGE_CPUUTIL_HIST readings.
	c.histAvgCpuUtil = c.ravg.Calc(&cpuUsageData{val: &c.cpuUtilization})

	return c.cpuUtilization
}

func (c *cpuUsageNode) String() string {
	// Converting to percentage.
	return strconv.FormatFloat(c.cpuUtilization*100.0, 'f', 3, 64)
}

var umlInstance *UtilizationMetricsListener

func utilizationMetricsListenerBaseInstance() *UtilizationMetricsListener {
	return &UtilizationMetricsListener{
		eventTypes:        []events.EventType{events.PCP},
		PCPHeaders:        make(map[string]map[string][]int),
		PowerUsageCluster: make(map[string]*powerUsageNode),
		CpuUsageCluster:   make(map[string]*cpuUsageNode),
		logsPersisted:     false,
		eventListeners:    make(map[events.EventType][]events.EventListener),
	}
}

// Window specifying the sample size of historic power readings for running average calculations.
const WINDOW_RUN_AVERAGE_POWER_HIST = 5

// Window specifying the sample size of historic cpu utilization readings for running average calculations.
const WINDOW_RUN_AVERAGE_CPUUTIL_HIST = 5

func NewUtilizationMetricsListener(powerSpecs string, lmt chan elecLogDef.LogMessageType,
	lmsg chan string) events.EventListener {
	if umlInstance != nil {
		return umlInstance
	}
	umlInstance = utilizationMetricsListenerBaseInstance()
	umlInstance.logMsgType = lmt
	umlInstance.logMsg = lmsg

	// Reading in the power specs.
	if file, err := os.Open(powerSpecs); err != nil {
		log.Fatal("Invalid PowerSpecs file provided!")
	} else {
		if err := json.NewDecoder(file).Decode(&umlInstance.PowerUsageCluster); err != nil {
			log.Fatal(errors.Wrap(err, "Incorrect format in power specs file!"))
		}
	}

	// For each host mentioned in the powerSpecs, monitoring CPU usage information.
	// Also, initializing running average calculator.
	for host := range umlInstance.PowerUsageCluster {
		umlInstance.CpuUsageCluster[host] = &cpuUsageNode{
			ravg: runAvg.Init(WINDOW_RUN_AVERAGE_CPUUTIL_HIST),
		}
		umlInstance.PowerUsageCluster[host].ravg = runAvg.Init(WINDOW_RUN_AVERAGE_POWER_HIST)
	}

	// Persisting headers.
	umlInstance.persistUtilizationMetricsHeaders()

	return umlInstance
}

func GetUtilizationMetricsListenerInstance() events.EventListener {
	return umlInstance
}

func (l *UtilizationMetricsListener) GetEventTypes() []events.EventType {
	return l.eventTypes
}

func (l *UtilizationMetricsListener) GetTotalPowerCpuHost(host string) (float64, error) {
	l.Lock()
	defer l.Unlock()
	if powerUsageHost, ok := l.PowerUsageCluster[host]; !ok {
		return 0.0, errors.New("No power usage metrics for host: " + host)
	} else {
		return powerUsageHost.totalPowerCpu, nil
	}
}

func (l *UtilizationMetricsListener) GetTotalPowerDramHost(host string) (float64, error) {
	l.Lock()
	defer l.Unlock()
	if powerUsageHost, ok := l.PowerUsageCluster[host]; !ok {
		return 0.0, errors.New("No power usage metrics for host: " + host)
	} else {
		return powerUsageHost.totalPowerDram, nil
	}
}

func (l *UtilizationMetricsListener) GetTDPHost(host string) (float64, error) {
	l.Lock()
	defer l.Unlock()
	if powerUsageHost, ok := l.PowerUsageCluster[host]; !ok {
		return 0.0, errors.New("No TDP data for host: " + host)
	} else {
		return powerUsageHost.TDPWatts, nil
	}
}

func (l *UtilizationMetricsListener) GetHistAvgPowerCpuHost(host string) (float64, error) {
	l.Lock()
	defer l.Unlock()
	if powerUsageHost, ok := l.PowerUsageCluster[host]; !ok {
		return 0.0, errors.New("No power usage history available for host: " + host)
	} else {
		return powerUsageHost.histAvgPowerCpu, nil
	}
}

func (l *UtilizationMetricsListener) GetCpuUtilHost(host string) (float64, error) {
	l.Lock()
	defer l.Unlock()
	if cpuUsageHost, ok := l.CpuUsageCluster[host]; !ok {
		return 0.0, errors.New("No cpu usage metrics for host: " + host)
	} else {
		return cpuUsageHost.cpuUtilization, nil
	}
}

func (l *UtilizationMetricsListener) GetHistAvgCpuUtilHost(host string) (float64, error) {
	l.Lock()
	defer l.Unlock()
	if cpuUsageHost, ok := l.CpuUsageCluster[host]; !ok {
		return 0.0, errors.New("No cpu usage history avaialable for host: " + host)
	} else {
		return cpuUsageHost.histAvgCpuUtil, nil
	}
}

func (l *UtilizationMetricsListener) Update(et events.EventType, data events.EventData) error {
	l.Lock()
	defer l.Unlock()
	switch et {
	case events.PCP:
		switch data.(type) {
		case *events.PCPEventData:
			pcpData := data.Get().(string)
			// Determining whether pcpData corresponds to the actual data or just the headers.
			if len(l.PCPHeaders) == 0 {
				// We have received the headers.
				headers := strings.Split(pcpData, ",")
				for i, header := range headers {
					components := strings.Split(header, ":")
					host := components[0]
					// Some headers are present for each thread/core/processor.
					// In this case, we just want to get the tag corresponding to the metric.
					// A header corresponding to a single processor might be shown as <tag>[<processor Id>].
					tag := strings.Split(components[1], "[")[0]
					if _, ok := l.PCPHeaders[host]; !ok {
						l.PCPHeaders[host] = map[string][]int{
							tag: []int{i},
						}
					} else {
						l.PCPHeaders[host][tag] = append(l.PCPHeaders[host][tag], i)
					}
				}
			} else {
				// allocMetricsListener := GetAllocationMetricsListener().(*AllocationMetricsListener)
				resUtil := make(events.ResourceUtilizationUpdateEventData)

				// We have received the data.
				pcpDataValues := strings.Split(pcpData, ",")
				for host, headerInfo := range l.PCPHeaders {
					resUtilHost := struct {
						TotalPowerCpu  float64
						TotalPowerDram float64
						CpuUtilization float64
					}{}
					cpuUtilHostFound := false
					powerUtilHostFound := false
					// Updating the CPU usage information for host.
					// user cpu time tag -> kernel.all.cpu.user
					// system cpu time tag -> kernel.all.cpu.sys
					// Note: if kernel.all.cpu.user and kernel.all.cpu.sys is not there, then CPU utilization
					// is not calculated.
					// Getting the location of the user and system cpu time for the given host.
					if userCpuTimeIndices, ok := headerInfo["kernel.all.cpu.user"]; ok {
						if sysCpuTimeIndices, ok := headerInfo["kernel.all.cpu.sys"]; ok {
							if idleCpuTimeIndices, ok := headerInfo["kernel.all.cpu.idle"]; ok {
								userCpuTime, err1 := strconv.ParseFloat(pcpDataValues[userCpuTimeIndices[0]], 64)
								sysCpuTime, err2 := strconv.ParseFloat(pcpDataValues[sysCpuTimeIndices[0]], 64)
								idleCpuTime, err3 := strconv.ParseFloat(pcpDataValues[idleCpuTimeIndices[0]], 64)
								if (err1 != nil) || (err2 != nil) || (err3 != nil) {
									// It is possible that the floating point numbers aren't
									// well formed.
									l.logMsgType <- elecLogDef.ERROR
									l.logMsg <- "Incorrect format in PCP data for CPU utilization!"
								} else {
									if _, ok := l.CpuUsageCluster[host]; ok {
										resUtilHost.CpuUtilization = l.CpuUsageCluster[host].
											update(userCpuTime, sysCpuTime, idleCpuTime)
										cpuUtilHostFound = true
									} else {
										// Specs not included for host in powerSpecs file.
										// Not monitoring power usage information for host.
									}
								}
							}
						}
					}

					// Updating the power usage information for host.
					// CPU power tag -> perfevent.hwcounters.rapl__RAPL_ENERGY_PKG.value
					// DRAM power tag -> perfevent.hwcounters.rapl__RAPL_ENERGY_DRAM.value
					// Total power for a given host would be the sum of the values for each socket on
					// that host.
					var totalCpuPowerRaw float64
					var totalDramPowerRaw float64
					if raplCpuPkgIndices, ok := headerInfo["perfevent.hwcounters.rapl__RAPL_ENERGY_PKG.value"]; ok {
						for _, index := range raplCpuPkgIndices {
							if p, err := strconv.ParseFloat(pcpDataValues[index], 64); err != nil {
								// It is possible that the floating point numbers aren't
								// well formed.
								l.logMsgType <- elecLogDef.ERROR
								l.logMsg <- "Incorrect format in PCP data for rapl energy package!"
							} else {
								totalCpuPowerRaw += p
							}
						}
					}
					if raplDramPkgIndices, ok := headerInfo["perfevent.hwcounters.rapl__RAPL_ENERGY_DRAM.value"]; ok {
						for _, index := range raplDramPkgIndices {
							if p, err := strconv.ParseFloat(pcpDataValues[index], 64); err != nil {
								// It is possible that the floating point numbers aren't
								// well formed.
								l.logMsgType <- elecLogDef.ERROR
								l.logMsg <- "Incorrect format in PCP data for rapl dram package!"
							} else {
								totalDramPowerRaw += p
							}
						}
					}

					// Updating the power usage information for host.
					if _, ok := l.PowerUsageCluster[host]; ok {
						powerCpu, powerDram := l.PowerUsageCluster[host].update(totalCpuPowerRaw, totalDramPowerRaw)
						resUtilHost.TotalPowerCpu = powerCpu
						resUtilHost.TotalPowerDram = powerDram
						powerUtilHostFound = true
					} else {
						// Specs not included for host in powerSpecs file.
						// Not monitoring power usage information for host.
					}
					// Updating resource utilization data to be sent to listeners (if any).
					if cpuUtilHostFound && powerUtilHostFound {
						resUtil[host] = resUtilHost
					}
				}
				// Logging cluster resource utilization information.
				l.persistClusterResourceUtilizationData()

				// Updating listeners with the latest resource utilization data for each host.
				l.NotifyListeners(events.RESOURCE_UTILIZATION_UPDATE, &resUtil)
			}
		default:
			return &events.InvalidDataForEventError{
				GivenType:    reflect.TypeOf(data),
				RequiredType: reflect.TypeOf(events.PCPEventData{}),
			}
		}

	default:
		return &events.InvalidEventTypeError{
			GivenType:     et,
			RequiredTypes: events.PCP.String(),
		}
	}
	return nil
}

func (l *UtilizationMetricsListener) String() string {
	// Placeholder
	return ""
}

// Headers for cluster resource utilization metrics.
// The header corresponds to the format of the data persisted.
const utilMetricsHeader = "<host1>:CPU_UTIL(%),CPU_POWER(Watts),DRAM_POWER(Watts)|<host2>..."

// Write headers.
func (l *UtilizationMetricsListener) persistUtilizationMetricsHeaders() {
	l.logMsgType <- elecLogDef.UTILIZATION_METRICS
	l.logMsg <- utilMetricsHeader
}

// Write cluster resource utilization data for the past second.
func (l *UtilizationMetricsListener) persistClusterResourceUtilizationData() {
	var buf bytes.Buffer
	values := []string{}
	for host := range l.PCPHeaders {
		utilValues := []string{}
		if cpuUsage, ok := l.CpuUsageCluster[host]; ok {
			// <CPU_UTIL>
			utilValues = append(utilValues, cpuUsage.String())
		}
		if powerUsage, ok := l.PowerUsageCluster[host]; ok {
			// <CPU_POWER>,<DRAM_POWER>
			utilValues = append(utilValues, powerUsage.String())
		}
		// Grouping all resource utilization metrics.
		// <CPU_UTIL>,<CPU_POWER>,<DRAM_POWER>
		utilValuesString := strings.Join(utilValues, ",")
		// Tagging the host corresponding to the resource utilization metrics.
		// <host>:<CPU_UTIL>,<CPU_POWER>,<DRAM_POWER>
		values = append(values, strings.Join([]string{host, utilValuesString}, ":"))
	}
	// Separating one host data from another's using '|' as the delimiter.
	// <host1>:...|<host2>...
	buf.WriteString(strings.Join(values, "|"))
	l.logMsgType <- elecLogDef.UTILIZATION_METRICS
	l.logMsg <- buf.String()
}

func (l *UtilizationMetricsListener) WriteLogs() {
	l.Lock()
	defer l.Unlock()
	if !l.logsPersisted {
		l.logMsgType <- elecLogDef.UTILIZATION_METRICS
		l.logMsg <- l.String()
		l.logsPersisted = true
	}
}

func (l *UtilizationMetricsListener) Stop() {
	// Writing logs in case they haven't been written.
	l.WriteLogs()
}

func (l *UtilizationMetricsListener) Attach(listener events.EventListener) {
	powerSpecs := make(events.PowerSpecsBroadcastEventData)

	for host, pu := range l.PowerUsageCluster {
		powerSpecs[host] = struct {
			IdlePowerWattsCpu    float64
			IdlePowerWattsDram   float64
			TDPWatts             float64
			MaxCpuDramPowerWatts float64
		}{
			(*pu).IdlePowerWattsCpu,
			(*pu).IdlePowerWattsDram,
			(*pu).TDPWatts,
			(*pu).MaxCpuDramPowerWatts,
		}
	}

	for _, et := range listener.GetEventTypes() {
		if _, ok := l.eventListeners[et]; ok {
			l.eventListeners[et] = append(l.eventListeners[et], listener)
		} else {
			l.eventListeners[et] = []events.EventListener{listener}
		}
	}

	// Notifying listeners about the power specifications of every host.
	l.NotifyListeners(events.POWER_SPECS_BROADCAST, &powerSpecs)
}

func (l *UtilizationMetricsListener) NotifyListeners(et events.EventType, d events.EventData) {
	go func() {
		for _, listener := range l.eventListeners[et] {
			if err := listener.Update(et, d); err != nil {
				l.LogError(err)
			}
		}
	}()
}

func (l *UtilizationMetricsListener) LogError(e error) {
	l.logMsgType <- elecLogDef.ERROR
	l.logMsg <- e.Error()
}
