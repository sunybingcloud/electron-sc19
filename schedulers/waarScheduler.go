package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/metrics"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/offerUtils"
	"errors"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	"github.com/mesos/mesos-go/api/v0/mesosutil"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"log"
	"sort"
)

// Watts as a Resource Scheduler.
// Attempt to schedule tasks such that the tolerance level will not be breached for the share
// of power that each task gets.
type WaaRScheduler struct {
	baseSchedPolicyState
	// Keeping track of the consumed resources for every offer.
	// OfferID => {cpu, mem}
	consumedResourcesInOffer map[string]*struct{ cpu, mem float64 }
}

// If a task is going to finish in the next N seconds, then the task is not considered for tolerance breach checks.
const futureNSecondsTaskFinishIn = 10

func (s *WaaRScheduler) remMem(offer *mesos.Offer) float64 {
	_, mem, _ := offerUtils.OfferAgg(offer)
	// Subtracting any used memory.
	if usedRes, ok := s.consumedResourcesInOffer[*offer.Id.Value]; ok {
		mem -= (*usedRes).mem
	}
	return mem
}

func (s *WaaRScheduler) remCPU(offer *mesos.Offer) float64 {
	cpu, _, _ := offerUtils.OfferAgg(offer)
	// Subtracting any used CPU.
	if usedRes, ok := s.consumedResourcesInOffer[*offer.Id.Value]; ok {
		cpu -= (*usedRes).cpu
	}
	return cpu
}

func (s *WaaRScheduler) canFitMem(offer *mesos.Offer, taskMem float64) bool {
	return s.remMem(offer) >= taskMem
}

func (s *WaaRScheduler) canFitCPU(offer *mesos.Offer, taskCPU float64) bool {
	return s.remCPU(offer) >= taskCPU
}

// Find the offer that is the best-fit in terms of memory for the given task.
// Return the index of the respective offer.
// Note: The offers should already have been sorted in non-decreasing order of Memory.
func (s *WaaRScheduler) bestFitMem(offers []*mesos.Offer, task def.Task) (int, error) {
	first := offers[0]
	last := offers[len(offers)-1]
	if s.canFitMem(first, task.RAM) {
		return 0, nil
	}
	if s.remMem(last) <= task.RAM {
		return -1, errors.New("No offer has sufficient memory for task!")
	}
	low := 0
	high := len(offers) - 1
	bestFitIndex := -1
	for low <= high {
		mid := (low + high) / 2
		unusedMemMid := s.remMem(offers[mid])
		if task.RAM < unusedMemMid {
			high = mid - 1
		} else if task.RAM > unusedMemMid {
			low = mid + 1
		} else {
			bestFitIndex = mid
			break
		}
	}

	// If the best fit index hasn't yet been found.
	if bestFitIndex == -1 {
		bestFitIndex = low
	}

	return bestFitIndex, nil
}

func (s *WaaRScheduler) updateConsumedResourcesOffer(offerID string, task def.Task) {
	if s.consumedResourcesInOffer == nil {
		s.consumedResourcesInOffer = make(map[string]*struct{ cpu, mem float64 })
	}
	if consumedResources, ok := s.consumedResourcesInOffer[offerID]; ok {
		(*consumedResources).cpu += task.CPU
		(*consumedResources).mem += task.RAM
	} else {
		s.consumedResourcesInOffer[offerID] = &struct {
			cpu, mem float64
		}{
			cpu: task.CPU,
			mem: task.RAM,
		}
	}
}

// Check if the tolerance for any of the already running tasks on the given host is going to be breached
// by launching the given task on the same host.
func (s *WaaRScheduler) toleranceBreached(
	spc SchedPolicyContext,
	host string,
	offer *mesos.Offer,
	resAllocInfo map[string]metrics.AllocationInfo,
	fitTasks map[string]metrics.FitTaskInfo,
	newTask def.Task) bool {

	// rml := metrics.GetRuntimeMetricsListenerInstance().(*metrics.RuntimeMetricsListener)
	totalCpuShare := newTask.CPU
	// Maintain taskIDs to be included for tolerance breach check.
	toInclude := make(map[string]bool)
	for taskID, allocInfo := range resAllocInfo {
		// Include task in tolerance breach check if not finishing in the immediate future.
		//if !rml.WillFinishIn(taskID, futureNSecondsTaskFinishIn, time.Now()) {
		toInclude[taskID] = true
		totalCpuShare += allocInfo.CpuShare
		//}
	}

	// Tasks that have just been fit into offers.
	for taskID, fitTaskInfo := range fitTasks {
		// Include task in tolerance breach check if not finishing in the immediate future.
		//if !rml.WillFinishIn(taskID, futureNSecondsTaskFinishIn, time.Now()) {
		toInclude[taskID] = true
		totalCpuShare += fitTaskInfo.CPU
		//}
	}

	utilMetrics := metrics.GetUtilizationMetricsListenerInstance().(*metrics.UtilizationMetricsListener)
	tdpHost, _ := utilMetrics.GetTDPHost(host)

	// Tolerance checks for the new task and all tasks that are either running or are yet
	// to start running on the host.

	// When launching new tasks on a node, the Thermal Design Power gives us a rough estimate of the
	// amount of power the CPU can consume if being completely utilized and running at full speed.
	// The power consumption of a host can approach the TDP, when additional tasks
	// are launched on it. In reality, it is very difficult to predict the power consumed by a set of tasks
	// running on a node.

	// Checking if the tolerance for the new task is going to be breached.
	baseSchedRef := spc.(*BaseScheduler)
	newTaskWatts, err := def.WattsToConsider(newTask, baseSchedRef.classMapWatts, offer)
	if err != nil {
		// Shouldn't be here.
		log.Println(err)
	}
	newTaskActualShareCpu := newTask.CPU / totalCpuShare
	newTaskActualSharePower := newTaskActualShareCpu * tdpHost
	if newTaskActualSharePower < (newTaskWatts * (1 - newTask.Tolerance)) {
		// Tolerance for new task will be breached if launched on given host.
		return true
	}

	// Checking if the tolerance for any of the already running tasks is going to be breached.
	for taskID, allocInfo := range resAllocInfo {
		if _, ok := toInclude[taskID]; ok {
			newActualShareCpu := allocInfo.CpuShare / totalCpuShare
			newActualSharePower := newActualShareCpu * tdpHost
			if newActualSharePower < allocInfo.MinAcceptablePower() {
				return true
			}
		}
	}

	// Checking if the tolerance for any of the fit tasks is going to be breached.
	for taskID, fitTaskInfo := range fitTasks {
		if _, ok := toInclude[taskID]; ok {
			newActualShareCpu := fitTaskInfo.CPU / totalCpuShare
			newActualSharePower := newActualShareCpu * tdpHost
			if newActualSharePower < (fitTaskInfo.Watts * (1 - fitTaskInfo.Tolerance)) {
				return true
			}
		}
	}

	// Tolerance not breached.
	return false
}

// For each task, let O[] be the set of offers that satisfy the CPU and Memory requirements.
// If the tolerance, for the currently running tasks and the tasks that have just been fit into
// offers, will not be breached by launching the new task, this offer is a candidate offer.
// If the tolerance is going to be breached tasks, skip the offer.
// For each of the candidate offers, determine the alignment score.
// Launch the task on the host corresponding to the offer with the best alignment score, so as to reduce cluster
// resource fragmentation.
func (s *WaaRScheduler) ConsumeOffers(spc SchedPolicyContext, driver sched.SchedulerDriver, offers []*mesos.Offer) {
	baseSchedRef := spc.(*BaseScheduler)
	baseSchedRef.LogOffersReceived(offers)

	// Updating the environment.
	for _, offer := range offers {
		offerUtils.UpdateEnvironment(offer)
	}

	// Declining all offers if scheduling has been shutdown.
	select {
	case <-baseSchedRef.ShutdownScheduling:
		for _, offer := range offers {
			baseSchedRef.LogNoPendingTasksDeclineOffers(offer)
			driver.DeclineOffer(offer.Id, mesosUtils.LongFilter)
		}
		baseSchedRef.LogNumberOfRunningTasks()
		return
	default:
	}

	allocMetrics := metrics.GetAllocationMetricsListener().(*metrics.AllocationMetricsListener)
	hostInfo := metrics.GetHostInfoListenerInstance().(*metrics.HostInfoListener)

	// Storing the Ids of the offers consumed.
	consumedOffers := make(map[string]bool)
	// Store the tasks to be scheduled and the offer ID of the consumed offer.
	tasksToSchedule := make(map[string][]*mesos.TaskInfo)

	for taskIndex := 0; taskIndex < len(baseSchedRef.tasks); taskIndex++ {
		task := baseSchedRef.tasks[taskIndex]
		// If there are remaining instances to be scheduled.
		if *task.Instances > 0 {
			// If scheduling policy switching enabled, then
			// stop scheduling if the #baseSchedRef.schedWindowSize tasks have been scheduled.
			if baseSchedRef.schedPolSwitchEnabled &&
				(s.numTasksScheduled >= baseSchedRef.schedWindowSize) {
				break // Offers will automatically get declined.
			}

			// Sorting the offers in non-decreasing order of memory.
			// Note: Resources contained in an offer change after every iteration. Hence, we sort
			// the offers after every iteration. This also ensures that we are fair to the next task.
			sort.SliceStable(offers, func(i, j int) bool {
				return s.remMem(offers[i]) <= s.remMem(offers[j])
			})
			// Finding the index of the best-fit offer for task's memory requirement.
			bestFitOfferMemIndex, err := s.bestFitMem(offers, task)
			if err != nil {
				// No offer has sufficient amount of memory.
				// Move onto next task.
				continue
			}

			// For each task, there can be more than one candidate offer that the task can fit into.
			// Among the candidate offers, the best one is picked.
			candidateOffersIndices := []int{}

			for offerIndex := bestFitOfferMemIndex; offerIndex < len(offers); offerIndex++ {
				if s.canFitCPU(offers[offerIndex], task.CPU) {
					hostname, err := hostInfo.GetHostname(*offers[offerIndex].SlaveId.Value)
					if err != nil {
						// This is the first time running into this offer. No other tasks have been
						// launched on this host.

						// Consider this offer as a potential candidate.
						//
						// If all the hosts in the cluster have a lower TDP than what the task needs,
						// we still should be able to launch the task (it might just take a little longer
						// to execute). Hence, not worrying about the available power.
						candidateOffersIndices = append(candidateOffersIndices, offerIndex)
					} else {
						// Other tasks can be running on the host.
						// Need to make sure that the already running tasks are not going to be affected.
						resAllocInfoHost, fitTasksInfoHost := allocMetrics.GetAllAllocationMetricsHost(hostname)
						if !s.toleranceBreached(spc, hostname, offers[offerIndex],
							resAllocInfoHost, fitTasksInfoHost, task) {
							// Consider this offer as a potential candidate.
							candidateOffersIndices = append(candidateOffersIndices, offerIndex)
						} else {
							// Tasks already running on the host corresponding to this offer
							// might get affected. Hence, not selecting this offer.
							// Move on to next offer.
						}
					}
				} else {
					// This offer is not a candidate offer - doesn't satisfy CPU requirement.
					// Move on to the next offer.
				}
			}

			// Determining the alignment scores for each of the candidate offers.
			chosenOfferIndex := -1
			var maxAlignmentScore float64
			firstIter := true
			for _, candidateOfferIndex := range candidateOffersIndices {
				score, _ := s.alignmentScore(
					task,
					offers[candidateOfferIndex],
					offerUtils.OfferResources{
						CPU: s.remCPU(offers[candidateOfferIndex]),
						RAM: s.remMem(offers[candidateOfferIndex]),
					})

				if firstIter {
					maxAlignmentScore = score
					firstIter = false
					chosenOfferIndex = candidateOfferIndex
				} else {
					if score > maxAlignmentScore {
						maxAlignmentScore = score
						chosenOfferIndex = candidateOfferIndex
					}
				}
			}

			// Fit task into chosen offer if present.
			if chosenOfferIndex != -1 {
				chosenOfferID := *offers[chosenOfferIndex].Id.Value
				baseSchedRef.LogCoLocatedTasks(offers[chosenOfferIndex].GetSlaveId().GoString())
				if _, ok := tasksToSchedule[chosenOfferID]; !ok {
					tasksToSchedule[chosenOfferID] = make([]*mesos.TaskInfo, 0, 0)
				}
				tasksToSchedule[chosenOfferID] = append(tasksToSchedule[chosenOfferID],
					baseSchedRef.newTaskInfo(offers[chosenOfferIndex], task,
						s.resourcesToReserve(spc, task, offers[chosenOfferIndex])))
				consumedOffers[chosenOfferID] = true
				// Updating consumed resources.
				s.updateConsumedResourcesOffer(chosenOfferID, task)
				// Another instance of task has been scheduled.
				*task.Instances--
				s.numTasksScheduled++

				if *task.Instances <= 0 {
					// All instances of task have been scheduled, remove it.
					baseSchedRef.tasks = append(baseSchedRef.tasks[:taskIndex],
						baseSchedRef.tasks[taskIndex+1:]...)

					if len(baseSchedRef.tasks) <= 0 {
						baseSchedRef.ExamineShutdownOfScheduling()
					}
				}
			}
		} else {
			// If here, then number of instances specified as 0 in the submitted workload.
			baseSchedRef.tasks = append(baseSchedRef.tasks[:taskIndex], baseSchedRef.tasks[taskIndex+1:]...)
			if len(baseSchedRef.tasks) <= 0 {
				baseSchedRef.ExamineShutdownOfScheduling()
			}
		}
	}

	for _, offer := range offers {
		if _, ok := consumedOffers[*offer.Id.Value]; ok {
			for _, t := range tasksToSchedule[*offer.Id.Value] {
				baseSchedRef.LogSchedTrace(t, offer)
			}
			LaunchTasks([]*mesos.OfferID{offer.Id}, tasksToSchedule[*offer.Id.Value], driver)
		} else {
			// Offer was not consumed.
			// Declining offer.
			cpus, mem, watts := offerUtils.OfferAgg(offer)
			baseSchedRef.LogInsufficientResourcesDeclineOffer(offer, cpus, mem, watts)
			driver.DeclineOffer(offer.Id, mesosUtils.DefaultFilter)
		}
	}

	// Clearing consumedResourcesInOffer so that future tasks are not affected.
	s.consumedResourcesInOffer = make(map[string]*struct{ cpu, mem float64 })
}

func (s *WaaRScheduler) resourcesToReserve(spc SchedPolicyContext, task def.Task, offer *mesos.Offer) []*mesos.Resource {

	resources := []*mesos.Resource{
		mesosutil.NewScalarResource("cpus", task.CPU),
		mesosutil.NewScalarResource("mem", task.RAM),
	}
	// Watts not used as a scalar resource in offers.
	return resources
}
