package schedulers

import (
	"bitbucket.org/sunybingcloud/electron/constants"
	"bitbucket.org/sunybingcloud/electron/def"
	"bitbucket.org/sunybingcloud/electron/events"
	elecLogDef "bitbucket.org/sunybingcloud/electron/logging/def"
	"bitbucket.org/sunybingcloud/electron/utilities"
	"bitbucket.org/sunybingcloud/electron/utilities/httpUtils"
	"bitbucket.org/sunybingcloud/electron/utilities/mesosUtils"
	mesos "github.com/mesos/mesos-go/api/v0/mesosproto"
	sched "github.com/mesos/mesos-go/api/v0/scheduler"
	"github.com/pkg/errors"
)

func coLocated(tasks map[string]bool, s BaseScheduler) {

	for task := range tasks {
		s.Log(elecLogDef.GENERAL, task)
	}

	s.Log(elecLogDef.GENERAL, "---------------------")
}

// Get the powerClass of the given hostname
func hostToPowerClass(hostName string) string {
	for powerClass, hosts := range constants.PowerClasses {
		if _, ok := hosts[hostName]; ok {
			return powerClass
		}
	}
	return ""
}

// Options to help initialize ElectronScheduler.
type SchedulerOptions func(e ElectronScheduler) error

// Options to help initialize Scheduling Policies.
type SchedPolicyOptions func(s SchedPolicyState) error

func WithAlignmentScoreFunction(functionName string) SchedPolicyOptions {
	return func(s SchedPolicyState) error {
		switch functionName {
		case "cpu-mem":
			s.AlignBy(alignByCpuAndMem)
		case "cpuUtil-mem":
			s.AlignBy(alignByCpuUtilAndMem)
		case "cpuPowerSlack":
			s.AlignBy(alignByCpuPowerSlack)
		case "weighted":
			s.AlignBy(weightedTaskOfferAlignment)
		default:
			// DEFAULT alignment function.
			s.AlignBy(equalWeightDefaultAlignment)
		}
		return nil
	}
}

func WithSchedPolicy(schedPolicyName string, params ...SchedPolicyOptions) SchedulerOptions {
	return func(s ElectronScheduler) error {
		if schedPolicy, ok := SchedPolicies[schedPolicyName]; !ok {
			return errors.New("Incorrect scheduling policy.")
		} else {
			schedPolicy.Init(params...)
			s.(*BaseScheduler).curSchedPolicy = schedPolicy
			return nil
		}
	}
}

func WithTasks(ts []def.Task) SchedulerOptions {
	return func(s ElectronScheduler) error {
		if ts == nil {
			return errors.New("Task[] is empty.")
		} else {
			s.(*BaseScheduler).tasks = ts
			// Recording the submission time of the given tasks.
			def.RecordSubmissionTime(s.(*BaseScheduler).tasks)
			return nil
		}
	}
}

func WithWattsAsAResource(waar bool) SchedulerOptions {
	return func(s ElectronScheduler) error {
		s.(*BaseScheduler).wattsAsAResource = waar
		return nil
	}
}

func WithClassMapWatts(cmw bool) SchedulerOptions {
	return func(s ElectronScheduler) error {
		s.(*BaseScheduler).classMapWatts = cmw
		return nil
	}
}

func WithRecordPCP(recordPCP *bool) SchedulerOptions {
	return func(s ElectronScheduler) error {
		s.(*BaseScheduler).RecordPCP = recordPCP
		return nil
	}
}

func WithShutdownScheduling(shutdown chan struct{}) SchedulerOptions {
	return func(s ElectronScheduler) error {
		if shutdown == nil {
			return errors.New("Shutdown channel is nil.")
		} else {
			s.(*BaseScheduler).ShutdownScheduling = shutdown
			return nil
		}
	}
}

func WithShutdownFramework(done chan struct{}) SchedulerOptions {
	return func(s ElectronScheduler) error {
		if done == nil {
			return errors.New("Done channel is nil.")
		} else {
			s.(*BaseScheduler).ShutdownFramework = done
			return nil
		}
	}
}

func WithHTTPServerShutdown(httpServerShutdown chan struct{}) SchedulerOptions {
	return func(s ElectronScheduler) error {
		s.(*BaseScheduler).HTTPServerShutdown = httpServerShutdown
		return nil
	}
}

func WithPCPLog(pcpLog chan struct{}) SchedulerOptions {
	return func(s ElectronScheduler) error {
		if pcpLog == nil {
			return errors.New("PCPLog channel is nil.")
		} else {
			s.(*BaseScheduler).PCPLog = pcpLog
			return nil
		}
	}
}

func WithLoggingChannels(lmt chan elecLogDef.LogMessageType, msg chan string) SchedulerOptions {
	return func(s ElectronScheduler) error {
		s.(*BaseScheduler).logMsgType = lmt
		s.(*BaseScheduler).logMsg = msg
		return nil
	}
}

func WithSchedPolSwitchEnabled(enableSchedPolicySwitch bool, switchingCriteria string) SchedulerOptions {
	return func(s ElectronScheduler) error {
		s.(*BaseScheduler).schedPolSwitchEnabled = enableSchedPolicySwitch
		// Checking if valid switching criteria.
		if _, ok := switchBasedOn[switchingCriteria]; !ok {
			return errors.New("Invalid scheduling policy switching criteria.")
		}
		s.(*BaseScheduler).schedPolSwitchCriteria = switchingCriteria
		return nil
	}
}

func WithNameOfFirstSchedPolToFix(nameOfFirstSchedPol string) SchedulerOptions {
	return func(s ElectronScheduler) error {
		if nameOfFirstSchedPol == "" {
			lmt := elecLogDef.WARNING
			msgColor := elecLogDef.LogMessageColors[lmt]
			msg := msgColor.Sprintf("First scheduling policy to deploy not mentioned. This is now going to be determined at runtime.")
			s.Log(lmt, msg)
			return nil
		}
		if _, ok := SchedPolicies[nameOfFirstSchedPol]; !ok {
			return errors.New("Invalid name of scheduling policy.")
		}
		s.(*BaseScheduler).nameOfFstSchedPolToDeploy = nameOfFirstSchedPol
		return nil
	}
}

func WithFixedSchedulingWindow(toFixSchedWindow bool, fixedSchedWindowSize int) SchedulerOptions {
	return func(s ElectronScheduler) error {
		if toFixSchedWindow {
			if fixedSchedWindowSize <= 0 {
				return errors.New("Invalid value of scheduling window size. Please provide a value > 0.")
			}
			lmt := elecLogDef.WARNING
			msgColor := elecLogDef.LogMessageColors[lmt]
			msg := msgColor.Sprintf("Fixing the size of the scheduling window to %d...", fixedSchedWindowSize)
			s.Log(lmt, msg)
			s.(*BaseScheduler).toFixSchedWindow = toFixSchedWindow
			s.(*BaseScheduler).schedWindowSize = fixedSchedWindowSize
		}
		// There shouldn't be any error for this scheduler option.
		// If toFixSchedWindow is set to false, then the fixedSchedWindowSize would be ignored. In this case,
		// 	the size of the scheduling window would be determined at runtime.
		return nil
	}
}

func WithTasksFromHTTPServer(tasksFromHTTP bool, port int) SchedulerOptions {
	return func(s ElectronScheduler) error {
		s.(*BaseScheduler).tasksFromHTTP = tasksFromHTTP
		if tasksFromHTTP {
			if err := httpUtils.ValidatePort(port); err != nil {
				return err
			}
			s.(*BaseScheduler).httpPort = port
		}
		return nil
	}
}

func WithEventListeners(listeners []events.EventListener) SchedulerOptions {
	return func(s ElectronScheduler) error {
		if s.(*BaseScheduler).eventListeners == nil {
			s.(*BaseScheduler).eventListeners = make(map[events.EventType][]events.EventListener)
		}
		for _, l := range listeners {
			s.(*BaseScheduler).Attach(l)
		}
		return nil
	}
}

// Launch tasks.
func LaunchTasks(offerIDs []*mesos.OfferID, tasksToLaunch []*mesos.TaskInfo, driver sched.SchedulerDriver) {
	driver.LaunchTasks(offerIDs, tasksToLaunch, mesosUtils.DefaultFilter)
	// Update resource availability
	for _, task := range tasksToLaunch {
		utilities.ResourceAvailabilityUpdate("ON_TASK_ACTIVE_STATE", *task.TaskId, *task.SlaveId)
	}
}

// Sort N tasks in the TaskQueue
func SortNTasks(tasks []def.Task, n int, sb def.SortBy) {
	def.SortTasks(tasks[:n], sb)
}
