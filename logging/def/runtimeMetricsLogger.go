package logging

type RuntimeMetricsLogger struct {
	loggerObserverImpl
}

func (ml RuntimeMetricsLogger) Log(message string) {
	if message != "" {
		ml.logObserverSpecifics[runtimeMetricsLogger].logFile.Println(message)
	}
}
