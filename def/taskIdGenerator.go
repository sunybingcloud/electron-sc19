package def

import (
	"github.com/satori/go.uuid"
	"strconv"
	"strings"
)

const TaskIDDelim = "-"
const TaskIDPrefix = "electron"

// Generate a unique task ID given the following,
// 1. Task configuration (name, instances, etc.)
func GenerateTaskID(task Task) string {
	var elements []string
	// Appending the framework name.
	elements = append(elements, TaskIDPrefix)
	// Appending the task name.
	elements = append(elements, task.Name)
	// Appending the instance.
	elements = append(elements, strconv.Itoa(*task.Instances))
	// Generating the UUID
	id := uuid.Must(uuid.NewV4())
	elements = append(elements, id.String())
	// section name for testing.
	// remove this.
	elements = append(elements, strings.Join(strings.Split(task.Section, " "), ""))
	// Joining elements with '-' as delimiter.
	return strings.Join(elements, TaskIDDelim)
}
